(function($){
var snakerflow = $.snakerflow;

$.extend(true, snakerflow.editors, {
	inputEditor : function(){
		var _props,_k,_div,_src,_r;
		this.init = function(props, k, div, src, r){
			_props=props; _k=k; _div=div; _src=src; _r=r;
			
			$('<input style="width:98%;"/>').val(props[_k].value).change(function(){
				props[_k].value = $(this).val();
			}).appendTo('#'+_div);
			
			$('#'+_div).data('editor', this);
		}
		this.destroy = function(){
			$('#'+_div+' input').each(function(){
				_props[_k].value = $(this).val();
			});
		}
	},
	selectEditor : function(arg){
		var _props,_k,_div,_src,_r;
		this.init = function(props, k, div, src, r){
			_props=props; _k=k; _div=div; _src=src; _r=r;

			var sle = $('<select  style="width:99%;"/>').val(props[_k].value).change(function(){
				props[_k].value = $(this).val();
			}).appendTo('#'+_div);
			
			if(typeof arg === 'string'){
				$.ajax({
				   type: "GET",
				   url: arg,
				   success: function(data){
					  var opts = eval(data);
					 if(opts && opts.length){
						for(var idx=0; idx<opts.length; idx++){
							sle.append('<option value="'+opts[idx].value+'">'+opts[idx].name+'</option>');
						}
						sle.val(_props[_k].value);
					 }
				   }
				});
			}else {
				for(var idx=0; idx<arg.length; idx++){
					sle.append('<option value="'+arg[idx].value+'">'+arg[idx].name+'</option>');
				}
				sle.val(_props[_k].value);
			}
			
			$('#'+_div).data('editor', this);
		};
		this.destroy = function(){
			$('#'+_div+' input').each(function(){
				_props[_k].value = $(this).val();
			});
		};
	},
	assigneeEditor : function(arg){
		var _props,_k,_div,_src,_r;
		this.init = function(props, k, div, src, r){
			_props=props; _k=k; _div=div; _src=src; _r=r;

			$('<input style="width:88%;" readonly="true" id="dialogEditor"/>').val(props[_k].value).appendTo('#'+_div);
			$('<input style="width:10%;" type="button" value="选择"/>').click(function(){
				//alert("选择:" + snakerflow.config.ctxPath + arg);
				var element = document.getElementById("dialogEditor");
				var iWidth = 800;
				var iHeight = 540;
				var iTop = (window.screen.height-iHeight)/2;
				var iLeft = (window.screen.width-iWidth)/2;
				var wd  = window.open(snakerflow.config.ctxPath + arg, " ", 'height='+iHeight+',innerHeight='+iHeight+',width='+iWidth+',innerWidth='+iWidth+',left='+iLeft+',top='+iTop+',toolbar=no,menubar=no,scrollbars=auto,resizeable=no,location=no,status=no,titlebar=0');
				if (wd) {
					window.focus();//判断窗口是否打开,如果打开,窗口前置
					winTimer = window.setInterval((function(){
						if(wd.closed){
							var returnValueTemp = window.returnVaule;//子窗体返回值
							if (returnValueTemp == null ) {
								window.clearInterval(winTimer);
								return;
							}
							var result = splitUsersAndAccounts(returnValueTemp);
							element.title = result[1];
							element.value = result[1];
							props[_k].value = result[1];
							props['assignee'].value = result[0];
							window.clearInterval(winTimer);
						}
					}), 500);
				}
			}).appendTo('#'+_div);

			$('#'+_div).data('editor', this);
		}
		this.destroy = function(){
			//
		}
	}
});

})(jQuery);