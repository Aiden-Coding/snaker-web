function selectOrg(ctx, e1, e2)
{
	var element1 = document.getElementById(e1);
	var element2 = document.getElementById(e2);
	var iWidth = 800;
	var iHeight = 540;
	var iTop = (window.screen.height-iHeight)/2;
	var iLeft = (window.screen.width-iWidth)/2;
	var wd  = window.open(ctx + "/dialogs/selectDialog.jsp?type=orgTree", " ", 'height='+iHeight+',innerHeight='+iHeight+',width='+iWidth+',innerWidth='+iWidth+',left='+iLeft+',top='+iTop+',toolbar=no,menubar=no,scrollbars=auto,resizeable=no,location=no,status=no,titlebar=0');
	if (wd) {
		window.focus();//判断窗口是否打开,如果打开,窗口前置
		winTimer = window.setInterval((function(){
			if(wd.closed){
				var returnValueTemp = window.returnVaule;//子窗体返回值
				if (returnValueTemp == null ) {
					window.clearInterval(winTimer);
					return;
				}
				var result = splitUsersAndAccounts(returnValueTemp);
				element1.value = result[0];
				element1.title = result[0];
				element2.value = result[1];
				window.clearInterval(winTimer);
			}
		}), 500);
	}
}

function selectOrgUser(ctx, e1, e2)
{
	var element1 = document.getElementById(e1);
	var element2 = document.getElementById(e2);
	var iWidth = 800;
	var iHeight = 540;
	var iTop = (window.screen.height-iHeight)/2;
	var iLeft = (window.screen.width-iWidth)/2;
	var wd  = window.open(ctx + "/dialogs/selectDialog.jsp?type=orgUserTree", " ", 'height='+iHeight+',innerHeight='+iHeight+',width='+iWidth+',innerWidth='+iWidth+',left='+iLeft+',top='+iTop+',toolbar=no,menubar=no,scrollbars=auto,resizeable=no,location=no,status=no,titlebar=0');
	if (wd) {
		window.focus();//判断窗口是否打开,如果打开,窗口前置
		winTimer = window.setInterval((function(){
			if(wd.closed){
				var returnValueTemp = window.returnVaule;//子窗体返回值
				if (returnValueTemp == null ) {
					window.clearInterval(winTimer);
					return;
				}
				var result = splitUsersAndAccounts(returnValueTemp);
				element1.value = result[0];
				element1.title = result[0];
				element2.value = result[1];
				window.clearInterval(winTimer);
			}
		}), 500);
	}
}

function splitUsersAndAccounts( userNamesAndAccount )
{
	var userNames = "";
	var accounts = "";

	var array = userNamesAndAccount.split( ";" );
	for(i=0; i<array.length; i++)
	{
		var temp = splitUserNameAndAccount(array[i]);
		userNames += temp[0] + ",";
		accounts += temp[1] + ",";
	}
	userNames = userNames.substr(0, userNames.length - 1);
	accounts = accounts.substr(0, accounts.length - 1);
	var result = new Array(2);
	result[0] = userNames;
	result[1] = accounts;
	return result;
}

function splitUserNameAndAccount( userNameAndAccount )
{
	var temp = new Array(2);
	if(userNameAndAccount.indexOf( "(" ) != -1)
	{
		temp[0] = userNameAndAccount.substring( 0,
      	userNameAndAccount.indexOf( "(" ) );
    	temp[1] = userNameAndAccount.substring( userNameAndAccount.indexOf( "(" ) + 1,
        userNameAndAccount.indexOf( ")" ) );
    }
    else
    {
    	temp[0] = userNameAndAccount;
    	temp[1] = userNameAndAccount;
    }
    return temp;
}